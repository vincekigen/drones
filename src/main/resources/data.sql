-- Seed states table
INSERT INTO states (id, name)
VALUES
    (1, 'IDLE'),
    (2, 'LOADING'),
    (3, 'DELIVERING'),
    (4, 'DELIVERED'),
    (5, 'RETURNING');

-- Seed models table
INSERT INTO models (id, name)
VALUES
    (1, 'Lightweight'),
    (2, 'Middleweight'),
    (3, 'Cruiserweight'),
    (4, 'Heavyweight');

-- Seed Drones
INSERT INTO drones (serial_number, model_id, weight_limit, battery_capacity, state_id)
VALUES
    ('TH456GHTYGFE', 1, 3000, 100, 1),
    ('GTR645DFSGSV', 1, 3000, 100, 1),
    ('YG54RFSCR6GS', 1, 3000, 100, 1),
    ('NHT353RSFCF6', 1, 3000, 100, 1),
    ('QW234SDSVXHS', 1, 3000, 100, 1),
    ('GDR564RTYGF4', 2, 5000, 100, 1),
    ('GDF35RDCFAY6', 2, 5000, 100, 1),
    ('NB4DFERDA6TG', 2, 5000, 100, 1),
    ('VCX3ESDS7GFE', 2, 5000, 100, 1),
    ('U76TFDS45ACD', 2, 5000, 100, 1),
    ('NJU765RFTY5D', 3, 7500, 100, 1),
    ('RDF53VVDHU5R', 3, 7500, 100, 1),
    ('VF4DSCA6GETA', 3, 7500, 100, 1),
    ('DS65FEYXG5AF', 3, 7500, 100, 1),
    ('HFT46FDCVA6A', 3, 7500, 100, 1),
    ('PO8HG54AFDVD', 4, 10000, 100, 1),
    ('KJ7YTFS54DAD', 4, 10000, 100, 1),
    ('DE45FSR543DZ', 4, 10000, 100, 1),
    ('DSEW45FAY67G', 4, 10000, 100, 1),
    ('YU7645FVDSG4', 4, 10000, 100, 1);

INSERT INTO medications (weight, code, name)
VALUES
    (550, 'G456DGSCF', 'Infacol'),
    (350, 'TY645SGVA', 'Paracetamol'),
    (1000, 'SFTE65RAF', 'Maramoja'),
    (250, 'GTSF5FDSR', 'Piriton'),
    (450, 'TE65AFSBF', 'Lyrica'),
    (100, 'SG65RFAVG', 'Ativan'),
    (770, 'YT5RDADCD', 'Ibuprofen'),
    (490, 'SR45EWDAS', 'Citalopram'),
    (310, 'DT77GGFAC', 'Onpattro'),
    (737, 'OU7TGSTFE', 'Naloxone'),
    (2522, 'SG65RDAF5', 'Narcan'),
    (127, 'G65EDSRAU', 'Tramadol'),
    (995, 'T5DADE4DA', 'Narcan');