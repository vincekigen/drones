package com.vincent.drone_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vincent.drone_api.entities.EDrone;

public interface DroneDAO extends JpaRepository<EDrone, Long> {
    
}
