package com.vincent.drone_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vincent.drone_api.entities.EState;

public interface StateDAO extends JpaRepository<EState, Integer> {
    
}
