package com.vincent.drone_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vincent.drone_api.entities.EModel;

public interface ModelDAO extends JpaRepository<EModel, Integer> {
    
}
