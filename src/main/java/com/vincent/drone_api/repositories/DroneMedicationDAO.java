package com.vincent.drone_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vincent.drone_api.entities.DroneMedicationPK;
import com.vincent.drone_api.entities.EDroneMedication;

public interface DroneMedicationDAO extends JpaRepository<EDroneMedication, DroneMedicationPK> {
    
}
