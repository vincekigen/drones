package com.vincent.drone_api.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vincent.drone_api.entities.EMedication;

public interface MedicationDAO extends JpaRepository<EMedication, Long> {
    
    Optional<EMedication> findByCode(String code);

    @Query(
        value = "SELECT * FROM medications "
            + "WHERE id = :medicationId "
            + "OR code = :medicationCode",
        nativeQuery = true
    )
    Optional<EMedication> findByIdOrCode(Long medicationId, String medicationCode);
}
