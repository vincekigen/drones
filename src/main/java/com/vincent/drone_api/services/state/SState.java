package com.vincent.drone_api.services.state;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vincent.drone_api.entities.EState;
import com.vincent.drone_api.repositories.StateDAO;

@Service
public class SState implements IState {

    @Autowired
    private StateDAO stateDAO;

    @Override
    public List<EState> getAll() {
        return stateDAO.findAll();
    }

    @Override
    public Optional<EState> getById(Integer id) {
        return stateDAO.findById(id);
    }
    
}
