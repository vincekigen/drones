package com.vincent.drone_api.services.state;

import java.util.List;
import java.util.Optional;

import com.vincent.drone_api.entities.EState;

public interface IState {
    
    List<EState> getAll();
    
    Optional<EState> getById(Integer id);
}
