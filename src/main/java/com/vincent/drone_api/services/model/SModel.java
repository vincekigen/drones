package com.vincent.drone_api.services.model;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vincent.drone_api.entities.EModel;
import com.vincent.drone_api.repositories.ModelDAO;

@Service
public class SModel implements IModel {

    @Autowired
    private ModelDAO modelDAO;

    @Override
    public List<EModel> getAll() {
        return modelDAO.findAll();
    }

    @Override
    public Optional<EModel> getById(Integer id) {
        return modelDAO.findById(id);
    }
    
}
