package com.vincent.drone_api.services.model;

import java.util.List;
import java.util.Optional;

import com.vincent.drone_api.entities.EModel;

public interface IModel {
    
    List<EModel> getAll();

    Optional<EModel> getById(Integer id);
}
