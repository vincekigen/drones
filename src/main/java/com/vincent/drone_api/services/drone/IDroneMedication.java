package com.vincent.drone_api.services.drone;

import com.vincent.drone_api.entities.EDrone;
import com.vincent.drone_api.entities.EDroneMedication;
import com.vincent.drone_api.entities.EMedication;

public interface IDroneMedication {
    
    EDroneMedication create(EDrone drone, EMedication medication);

    void save(EDroneMedication droneMedication);
}
