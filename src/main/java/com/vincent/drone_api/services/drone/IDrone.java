package com.vincent.drone_api.services.drone;

import java.util.List;
import java.util.Optional;

import com.vincent.drone_api.dtos.drone.DroneDTO;
import com.vincent.drone_api.dtos.drone.DroneMedicationDTO;
import com.vincent.drone_api.entities.EDrone;

public interface IDrone {
    
    void addMedications(EDrone drone, List<DroneMedicationDTO> medicationList);
    
    EDrone create(DroneDTO droneDTO);

    List<EDrone> getAll();
    
    Optional<EDrone> getById(Long id);

    void save(EDrone drone);

    EDrone update(EDrone drone, DroneDTO droneDTO);
}
