package com.vincent.drone_api.services.drone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vincent.drone_api.entities.EDrone;
import com.vincent.drone_api.entities.EDroneMedication;
import com.vincent.drone_api.entities.EMedication;
import com.vincent.drone_api.repositories.DroneMedicationDAO;

@Service
public class SDroneMedication implements IDroneMedication {

    @Autowired
    private DroneMedicationDAO droneMedicationDAO;

    @Override
    public EDroneMedication create(EDrone drone, EMedication medication) {
        EDroneMedication droneMedication = new EDroneMedication(drone, medication);
        save(droneMedication);
        return droneMedication;
    }

    @Override
    public void save(EDroneMedication droneMedication) {
        droneMedicationDAO.save(droneMedication);
    }
    
}
