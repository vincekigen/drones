package com.vincent.drone_api.services.drone;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vincent.drone_api.dtos.drone.DroneDTO;
import com.vincent.drone_api.dtos.drone.DroneMedicationDTO;
import com.vincent.drone_api.entities.EDrone;
import com.vincent.drone_api.entities.EDroneMedication;
import com.vincent.drone_api.entities.EMedication;
import com.vincent.drone_api.entities.EModel;
import com.vincent.drone_api.entities.EState;
import com.vincent.drone_api.exceptions.InvalidInputException;
import com.vincent.drone_api.repositories.DroneDAO;
import com.vincent.drone_api.services.medication.IMedication;
import com.vincent.drone_api.services.model.IModel;
import com.vincent.drone_api.services.state.IState;

@Service
public class SDrone implements IDrone {

    Logger logger = LoggerFactory.getLogger(SDrone.class);

    @Autowired
    private DroneDAO droneDAO;

    @Autowired
    private IDroneMedication sDroneMedication;

    @Autowired
    private IMedication sMedication;

    @Autowired
    private IModel sModel;

    @Autowired
    private IState sState;

    @Override
    public void addMedications(EDrone drone, List<DroneMedicationDTO> medicationList) {
        if (medicationList == null || medicationList.isEmpty()) { return; }

        // Get the drone's weight limit
        Float weightLimit = drone.getWeightLimit();
        Float loadWeight = 0.0f;

        List<EDroneMedication> droneMedications = new ArrayList<>();
        for (DroneMedicationDTO droneMedicationDTO : medicationList) {
            try {
                EMedication medication = sMedication.getByIdOrCode(droneMedicationDTO.getMedicationCode(), true);

                loadWeight += medication.getWeight();
                if (loadWeight > weightLimit) {
                    throw new InvalidInputException("drone weight limit exceeded", "droneWeight");
                }

                EDroneMedication droneMedication = sDroneMedication.create(drone, medication);
                droneMedications.add(droneMedication);
            } catch (Exception e) {
                logger.error("\nERROR: SDrone.addMedications | [MSG] - {}", e.getMessage());
                continue;
            }
        }

        drone.setMedications(droneMedications);
    }

    /**
     * Creates a drone obj
     */
    @Override
    public EDrone create(DroneDTO droneDTO) {
        
        EDrone drone = new EDrone();
        drone.setBatteryCapacity(100);
        setDroneModel(drone, droneDTO.getModelId());
        setDroneState(drone, droneDTO.getStateId());
        drone.setSerialNumber(droneDTO.getSerialNumber());
        drone.setWeightLimit(droneDTO.getWeightLimit());

        save(drone);
        return drone;
    }

    @Override
    public List<EDrone> getAll() {
        return droneDAO.findAll();
    }

    @Override
    public Optional<EDrone> getById(Long id) {
        return droneDAO.findById(id);
    }

    /**
     * Persists the drone obj to db
     */
    @Override
    public void save(EDrone drone) {
        droneDAO.save(drone);
    }

    /**
     * Sets the drone's model
     * @param drone
     * @param modelId
     */
    public void setDroneModel(EDrone drone, Integer modelId) {
        if (modelId == null) { return; }

        Optional<EModel> model = sModel.getById(modelId);
        if (!model.isPresent()) {
            throw new InvalidInputException("model with specified id not found", "modelId");
        }
        drone.setModel(model.get());
    }

    /**
     * Sets the drone's state
     * @param drone
     * @param stateId
     */
    public void setDroneState(EDrone drone, Integer stateId) {
        if (stateId == null) { return; }

        Optional<EState> state = sState.getById(stateId);
        if (!state.isPresent()) {
            throw new InvalidInputException("state with specified id not found", "stateId");
        }
        drone.setState(state.get());
    }

    /**
     * Updates the drone obj
     */
    @Override
    public EDrone update(EDrone drone, DroneDTO droneDTO) {

        if (droneDTO.getBatteryCapacity() != null) {
            drone.setBatteryCapacity(droneDTO.getBatteryCapacity());
        }
        if (droneDTO.getWeightLimit() != null) {
            drone.setWeightLimit(droneDTO.getWeightLimit());
        }

        setDroneModel(drone, droneDTO.getModelId());
        setDroneState(drone, droneDTO.getStateId());

        save(drone);
        return drone;
    }
    
}
