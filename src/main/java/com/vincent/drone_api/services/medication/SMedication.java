package com.vincent.drone_api.services.medication;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vincent.drone_api.dtos.medication.MedicationDTO;
import com.vincent.drone_api.entities.EMedication;
import com.vincent.drone_api.exceptions.NotFoundException;
import com.vincent.drone_api.repositories.MedicationDAO;

@Service
public class SMedication implements IMedication {

    Logger logger = LoggerFactory.getLogger(SMedication.class);

    @Autowired
    private MedicationDAO medicationDAO;

    /**
     * Create medication obj
     */
    @Override
    public EMedication create(MedicationDTO medicationDTO) {
        
        EMedication medication = new EMedication();
        medication.setCode(medicationDTO.getCode());
        medication.setImage(medicationDTO.getImage());
        medication.setName(medicationDTO.getName());
        medication.setWeight(medicationDTO.getWeight());

        save(medication);
        return medication;
    }

    /**
     * Get a list of medication objects
     */
    @Override
    public List<EMedication> getAll() {
        return medicationDAO.findAll();
    }

    /**
     * Get medication obj by code
     */
    @Override
    public Optional<EMedication> getByCode(String code) {
        return medicationDAO.findByCode(code);
    }

    /**
     * Get medication obj by id
     */
    @Override
    public Optional<EMedication> getById(Long medicationId) {
        return medicationDAO.findById(medicationId);
    }

    @Override
    public Optional<EMedication> getByIdOrCode(String medicationValue) {
        Long medicationId;
        try {
            medicationId = Long.valueOf(medicationValue);
        } catch (NumberFormatException ex) {
            medicationId = (Long) null;
            logger.info("\nERROR: [SMedication.getByIdOrCode] | [MSG] - {}", ex.getMessage());
            return getByCode(medicationValue);
        }

        return medicationDAO.findByIdOrCode(medicationId, medicationValue);
    }

    @Override
    public EMedication getByIdOrCode(String medicationValue, Boolean handleNotFound) {
        Optional<EMedication> medication = getByIdOrCode(medicationValue);
        if (!medication.isPresent() && handleNotFound) {
            throw new NotFoundException("medication with specified id or code not found", "medicationValue");
        }

        return medication.get();
    }

    /**
     * Persist medication obj to db
     */
    @Override
    public void save(EMedication medication) {
        medicationDAO.save(medication);
    }

    /**
     * Update medication obj
     */
    @Override
    public EMedication update(EMedication medication, MedicationDTO medicationDTO) {
        if (medicationDTO.getCode() != null) {
            medication.setCode(medicationDTO.getCode());
        }
        if (medicationDTO.getImage() != null) {
            medication.setImage(medicationDTO.getImage());
        }
        if (medicationDTO.getName() != null) {
            medication.setName(medicationDTO.getName());
        }
        if (medicationDTO.getWeight() != null) {
            medication.setWeight(medicationDTO.getWeight());
        }
        
        save(medication);
        return medication;
    }
    
}
