package com.vincent.drone_api.services.medication;

import java.util.List;
import java.util.Optional;

import com.vincent.drone_api.dtos.medication.MedicationDTO;
import com.vincent.drone_api.entities.EMedication;

public interface IMedication {
    
    EMedication create(MedicationDTO medicationDTO);

    List<EMedication> getAll();

    Optional<EMedication> getByCode(String code);
    
    Optional<EMedication> getById(Long medicationId);

    Optional<EMedication> getByIdOrCode(String medicationValue);

    EMedication getByIdOrCode(String medicationValue, Boolean handleNotFound);

    void save(EMedication medication);

    EMedication update(EMedication medication, MedicationDTO medicationDTO);
}
