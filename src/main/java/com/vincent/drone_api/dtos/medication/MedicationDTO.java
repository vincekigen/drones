package com.vincent.drone_api.dtos.medication;

import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vincent.drone_api.entities.EMedication;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class MedicationDTO {
    
    private Long id;

    @Pattern(
        regexp = "^[A-Za-z0-9-_]+$", 
        message = "Invalid value; allowable values include letters, numbers, hyphen and underscore"
    )
    private String name;

    private Float weight;

    @Pattern(
        regexp = "^[A-Z0-9_]+$",
        message = "Invalid value; allowable values include uppercase letters, underscore and numbers"
    )
    private String code;

    private byte[] image;

    public MedicationDTO(EMedication medication) {
        this.setId(medication.getId());
        this.setName(medication.getName());
        this.setWeight(medication.getWeight());
        this.setCode(medication.getCode());
        this.setImage(medication.getImage());
    }
}
