package com.vincent.drone_api.dtos.drone;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vincent.drone_api.entities.EDrone;
import com.vincent.drone_api.entities.EDroneMedication;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class DroneDTO {
    
    private Long id;

    @Size(
        min = 5,
        max = 100)
    private String serialNumber;

    private String model;

    private Integer modelId;

    private Float weightLimit;

    private Integer batteryCapacity;

    private String state;

    private Integer stateId;

    private List<DroneMedicationDTO> medications;

    public DroneDTO(EDrone drone) {
        this.setId(drone.getId());
        this.setSerialNumber(drone.getSerialNumber());
        setMedicationData(drone.getMedications());
        this.setModel(drone.getModel().getName());
        this.setWeightLimit(drone.getWeightLimit());
        this.setBatteryCapacity(drone.getBatteryCapacity());
        this.setState(drone.getState().getName());
    }

    public void setMedicationData(List<EDroneMedication> droneMedicationList) {
        if (droneMedicationList == null || droneMedicationList.isEmpty()) { return; }

        medications = new ArrayList<>();
        for (EDroneMedication droneMedication : droneMedicationList) {
            medications.add(new DroneMedicationDTO(droneMedication));
        }
    }
}
