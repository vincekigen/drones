package com.vincent.drone_api.dtos.drone;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.vincent.drone_api.dtos.medication.MedicationDTO;
import com.vincent.drone_api.entities.EDroneMedication;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(value = Include.NON_NULL)
public class DroneMedicationDTO {
    
    private MedicationDTO medication;

    private Long medicationId;

    private String medicationCode;

    public DroneMedicationDTO(EDroneMedication droneMedication) {
        setMedication(new MedicationDTO(droneMedication.getMedication()));
    }
}
