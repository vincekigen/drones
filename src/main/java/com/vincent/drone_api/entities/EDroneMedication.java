package com.vincent.drone_api.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "drone_medications")
@Data
@NoArgsConstructor
public class EDroneMedication implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private DroneMedicationPK droneMedicationPK;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "drone_id", referencedColumnName = "id")
    @MapsId(value = "droneId")
    private EDrone drone;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medication_id", referencedColumnName = "id")
    @MapsId(value = "medicationId")
    private EMedication medication;

    public EDroneMedication(EDrone drone, EMedication medication) {
        setDrone(drone);
        setMedication(medication);
        setDroneMedicationPK(new DroneMedicationPK(drone.getId(), medication.getId()));
    }
}
