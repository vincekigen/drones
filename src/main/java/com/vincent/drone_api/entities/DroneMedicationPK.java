package com.vincent.drone_api.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Embeddable
public class DroneMedicationPK implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Column(name = "drone_id")
    private long droneId;

    @Column(name = "medication_id")
    private long medicationId;

    public DroneMedicationPK(long droneId, long medicationId) {
        this.setDroneId(droneId);
        this.setMedicationId(medicationId);
    }
}
