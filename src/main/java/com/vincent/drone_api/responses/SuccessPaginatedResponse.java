package com.vincent.drone_api.responses;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(value = {"contentMap"})
public class SuccessPaginatedResponse {

    private int status = 200;

    private String message;

    private Object content;

    Map<String, Object> contentMap = new HashMap<>();

    public SuccessPaginatedResponse(
            int status, 
            String message, 
            List<?> dataArray, 
            Class<?> customClass, 
            Class<?> entityClass
            ) throws InstantiationException, IllegalAccessException, IllegalArgumentException, 
                InvocationTargetException, NoSuchMethodException, SecurityException {

        this.setStatus(status);
        this.setMessage(message);

        List<Object> contentList = new ArrayList<>();
        for (Object objectItem : dataArray) {
            Object customObj = customClass.getConstructor(entityClass).newInstance(objectItem);
            contentList.add(customObj);
        }
        contentMap.put("data", contentList);
        this.setContent(contentMap);
    }
    
}
