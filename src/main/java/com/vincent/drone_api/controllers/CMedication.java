package com.vincent.drone_api.controllers;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vincent.drone_api.dtos.medication.MedicationDTO;
import com.vincent.drone_api.entities.EMedication;
import com.vincent.drone_api.exceptions.NotFoundException;
import com.vincent.drone_api.responses.SuccessPaginatedResponse;
import com.vincent.drone_api.responses.SuccessResponse;
import com.vincent.drone_api.services.medication.IMedication;

@RestController
public class CMedication {
    
    @Autowired
    private IMedication sMedication;

    @PostMapping(path = "/medication", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SuccessResponse> createMedication(@Valid @RequestBody MedicationDTO medicationDTO) {

        EMedication medication = sMedication.create(medicationDTO);

        return ResponseEntity
            .ok()
            .body(new SuccessResponse(201, "successfully created medication", new MedicationDTO(medication)));
    }

    @GetMapping(path = "/medication", produces = "application/json")
    public ResponseEntity<SuccessPaginatedResponse> getAll() throws InstantiationException, IllegalAccessException, 
            IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {

        List<EMedication> medications = sMedication.getAll();

        return ResponseEntity
            .ok()
            .body(new SuccessPaginatedResponse(200, "successfully fetched medications", 
                medications, MedicationDTO.class, EMedication.class));
    }

    @GetMapping(path = "/medication/code/{code}", produces = "application/json")
    public ResponseEntity<SuccessResponse> getByCode(@PathVariable String code) {

        Optional<EMedication> medication = sMedication.getByCode(code);
        if (!medication.isPresent()) {
            throw new NotFoundException("medication with specified code not found", "medicationCode");
        }

        return ResponseEntity
            .ok()
            .body(new SuccessResponse(200, "successfully fetched medication", new MedicationDTO(medication.get())));
    }

    @GetMapping(path = "/medication/{id}", produces = "application/json")
    public ResponseEntity<SuccessResponse> getById(@PathVariable Long id) {

        Optional<EMedication> medication = sMedication.getById(id);
        if (!medication.isPresent()) {
            throw new NotFoundException("mediction with specified id not found", "medicationId");
        }

        return ResponseEntity
            .ok()
            .body(new SuccessResponse(200, "successfully fetched medication", new MedicationDTO(medication.get())));
    }

    @PatchMapping(path = "/medication/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SuccessResponse> updateMedication(@PathVariable Long id, @Valid @RequestBody MedicationDTO medicationDTO) {

        Optional<EMedication> medicationOpt = sMedication.getById(id);
        if (!medicationOpt.isPresent()) {
            throw new NotFoundException("medication with specified id not found", "medicationId");
        }

        EMedication medication = sMedication.update(medicationOpt.get(), medicationDTO);

        return ResponseEntity
            .ok()
            .body(new SuccessResponse(200, "successfully updated medication", new MedicationDTO(medication)));
    }
}
