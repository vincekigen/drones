package com.vincent.drone_api.controllers;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vincent.drone_api.dtos.drone.DroneDTO;
import com.vincent.drone_api.entities.EDrone;
import com.vincent.drone_api.exceptions.NotFoundException;
import com.vincent.drone_api.responses.SuccessPaginatedResponse;
import com.vincent.drone_api.responses.SuccessResponse;
import com.vincent.drone_api.services.drone.IDrone;

@RestController
public class CDrone {
    
    @Autowired
    private IDrone sDrone;

    @PostMapping(path = "/drone", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SuccessResponse> registerDrone(@Valid @RequestBody DroneDTO droneDTO) throws URISyntaxException {

        EDrone drone = sDrone.create(droneDTO);

        return ResponseEntity
            .created(new URI("/drone/" + drone.getId()))
            .body(new SuccessResponse(201, "successfully created drone", new DroneDTO(drone)));
    }

    @PostMapping(path = "/drone/{droneId}/medication", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SuccessResponse> addMedications(@PathVariable Long droneId, @RequestBody DroneDTO droneDTO) {

        Optional<EDrone> drone = sDrone.getById(droneId);
        if (!drone.isPresent()) {
            throw new NotFoundException("drone with specified id not found", "droneId");
        }
        sDrone.addMedications(drone.get(), droneDTO.getMedications());

        return ResponseEntity
            .ok()
            .body(new SuccessResponse(200, "successfully added medication(s)", new DroneDTO(drone.get())));
    }

    @GetMapping(path = "/drone/{droneId}", produces = "application/json")
    public ResponseEntity<SuccessResponse> getById(@PathVariable Long droneId) {
        
        Optional<EDrone> drone = sDrone.getById(droneId);
        if (!drone.isPresent()) {
            throw new NotFoundException("drone with specified id not found", "droneId");
        }

        return ResponseEntity
            .ok()
            .body(new SuccessResponse(200, "successfully fetched drone", new DroneDTO(drone.get())));
    }

    @GetMapping(path = "/drone", produces = "application/json")
    public ResponseEntity<SuccessPaginatedResponse> getDronesList() 
            throws InstantiationException, IllegalAccessException, IllegalArgumentException, 
            InvocationTargetException, NoSuchMethodException, SecurityException {

        List<EDrone> drones = sDrone.getAll();

        return ResponseEntity
            .ok()
            .body(new SuccessPaginatedResponse(200, "successfully fetched drones", drones, 
                DroneDTO.class, EDrone.class));
    }

    @PatchMapping(path = "/drone/{droneId}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<SuccessResponse> updateDrone(@PathVariable Long droneId, @RequestBody DroneDTO droneDTO) {

        Optional<EDrone> droneOpt = sDrone.getById(droneId);
        if (!droneOpt.isPresent()) {
            throw new NotFoundException("drone with specified id not found", "droneId");
        }

        EDrone drone = sDrone.update(droneOpt.get(), droneDTO);

        return ResponseEntity
            .ok()
            .body(new SuccessResponse(200, "successfully updated drone", new DroneDTO(drone)));
    }
}
